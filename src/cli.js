import fs from 'fs';
import path from 'path';

import arg from 'arg';
import svg_to_png from 'svg-to-png';
import swig from 'swig';

const HEX_CHARS = '0123456789ABCDEF';
const TEMPLATES = ['default'];

function getRandomHEXColor() {
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += HEX_CHARS[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getColor(color) {
    if (color) {
        if (color === 'r' || color === 'random') {
            return getRandomHEXColor();
        }
        return color;
    }
    return '#000000';
}

function parseArgumentsIntoOptions(rawArgs) {
    const args = arg(
        {
            '--color': String,
            '--template': String,
            '-t': '--template',
            '-c': '--color'
        },
        {
            argv: rawArgs.slice(2)
        }
    );
    return {
        outputPath: args._[0] || 'tmp.svg',
        label: args._[1] || 'no label',
        value: args._[2] || 'no value',
        color: getColor(args['--color']),
        template: TEMPLATES.includes(args['--template']) ? args['--template'] : TEMPLATES[0]
    };
}

export function cli(args) {
    let options = parseArgumentsIntoOptions(args);
    let icon = swig.renderFile(path.join(__dirname, `./templates/${options.template}.svg`), options);

    const tmpPath = path.join(__dirname, 'icon.svg');
    const outputPath = path.join(path.resolve('./'), ...options.outputPath.split('/'));
    fs.writeFileSync(tmpPath, icon, (error) => {
        if (error) {
            throw new Error(error);
        }
    });

    svg_to_png.convert(tmpPath, outputPath).then(() => {});

    console.log(options);
}
