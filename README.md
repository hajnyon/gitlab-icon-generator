# Gitlab icon generator

## Usage

```bash
npm install -g gitlab-icon-generator
gig <PATH_TO_OUTPUT_FOLDER> <LABEL> <SUB_LABEL>
```

```bash
npx --yes gitlab-icon-generator ./ Label SubLabel -c r
```

### Options

| options            | values                              | default   |
| :----------------- | :---------------------------------- | :-------- |
| `-c`, `--color`    | `r` for random color, hex string    | `#000000` |
| `-t`, `--template` | template name from templates folder | `default` |

## Credits

https://www.twilio.com/blog/how-to-build-a-cli-with-node-js
